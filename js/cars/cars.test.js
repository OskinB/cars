document.body.innerHTML = '<div id="main"></div>';
var { createStory, renderStory } = require('../scripts.js')

test('There should be a <p> element inside the main div', function(){
    var getFromDom = function(){
        renderStory();
        return document.querySelector("#main").innerHTML;
    }
    expect(getFromDom() ).toMatch(/<p>/)
});

test('There should be an <img> with the car of your choice', function(){
    var getFromDom = function(){
        renderStory();
        return document.querySelector("#main").innerHTML;
    }
    expect(getFromDom() ).toMatch(/<img/)
});

test('Every sentence should start with "This is a"', function(){
    var getFromDom = function(){
        renderStory();
        return document.querySelector("#main").innerHTML;
    }
    expect(getFromDom() ).toMatch(/This is a/)
});